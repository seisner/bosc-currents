import numpy as np
import os
os.environ['NUMEXPR_MAX_THREADS'] = '24'
os.environ['NUMEXPR_NUM_THREADS'] = '18'
import numexpr as ne
from timeit import timeit

a = np.random.rand(274,720,1440)
b = np.random.rand(274,720,1440)
start = timeit()
for i in range(10):
    a*2 + b**2
    print(i)
end = timeit()
print(end - start)

start = timeit()
for i in range(10):
    ne.evaluate('a*2 + b**2')
    print(i)
end = timeit()
print(timeit())
