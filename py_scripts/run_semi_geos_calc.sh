#!/bin/bash

vers="$1"

data_dir="/mnt/f/bosc-currents/carton_lab/full_year/"

sg_file_base="rads_global_nrt_sla_2019.nc"
write_file_base="global_BOSC_SGE_2Lev_2019.nc"

sg_file="${data_dir}${sg_file_base}"
write_file="${data_dir}${write_file_base}"

sg_file_new="${data_dir}v${vers}_${sg_file_base}"
write_file_new="${data_dir}v${vers}_${write_file_base}"





ncks -x -v ugsg,vgsg $sg_file $sg_file_new

ncks -x -v u_20cm,v_20cm,u_15m,v_15m $write_file $write_file_new


echo "Running py script..."

python semi_geos_calc.py -e "${data_dir}era5-stress-dy-2019_remap.nc" -s "${data_dir}era5-stokes-dy-2019_regrid.nc" -g "${sg_file_new}" -w "${write_file_new}"


