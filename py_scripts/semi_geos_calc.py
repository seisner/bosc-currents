import os
import netCDF4 as nc
import numpy as np
import matplotlib.pyplot as plt
import matplotlib
import cartopy.crs as ccrs
from scipy import signal
from scipy import interpolate
import random
import tensorflow as tf
import keras
import cmocean
from sklearn.metrics import r2_score
import tensorflow.keras as keras
from keras.models import Sequential, Model, Input
from keras.layers import Dense,Dropout,LeakyReLU,BatchNormalization,concatenate
from keras.callbacks import ModelCheckpoint
from keras.callbacks import TensorBoard
from keras.callbacks import EarlyStopping
import getopt
import sys

os.environ['TF_XLA_FLAGS'] = '--tf_xla_enable_xla_devices'
os.environ["OMP_NUM_THREADS"] = '18'

data_directory = '/mnt/f/bosc-currents/carton_lab/full_year'
image_directory = '/home/seisner/bosc-currents/carton_lab/images'
model_weights = '/home/seisner/bosc-currents/carton_lab/NN_model_weights'


try:

    options = "w:g:s:e:"

    argumentList = sys.argv[1:]

    arguments, values = getopt.getopt(argumentList, options)

    for currentArgument, currentValue in arguments:

        if currentArgument in ("-w"):
            w_file = str(currentValue)

        if currentArgument in ("-g"):
            g_file = str(currentValue)
            print(g_file)

        if currentArgument in ("-e"):
            e_file = str(currentValue)

        if currentArgument in ("-s"):
            s_file = str(currentValue)

except getopt.error as err:

    print(str(err))





def ll2ind(coord, latlonarray):
    abs_list = [abs(x - coord) for x in latlonarray]
    ind = np.argmin(abs_list)
    return ind

def make_plot(imgname,title,lon,lat,dvar,clim,cmap):

    ax = plt.axes(projection=ccrs.PlateCarree(central_longitude=180))
    

    lon[lon<0] = lon[lon < 0] + 360.0
    minv  = clim[0]
    maxv  = clim[1]
    ticks = np.linspace(minv,maxv,10)
    plt.scatter(lon,lat,c=dvar,cmap=cmap, vmin=minv, vmax=maxv, transform=ccrs.PlateCarree(), marker='.')
    ax.coastlines()
    plt.title(title)
    plt.savefig(os.path.join(image_directory,imgname))
    plt.close('all')

def mround(num_array, n_mantis):
    flat_num_array = num_array.flatten()
    out_array = num_array
    for i in range(len(flat_num_array)):
        index = np.unravel_index(i, np.shape(num_array))
        try:
            out_array[index[0],index[1],index[2]] = float(str(flat_num_array[i]).split('e')[0][0:n_mantis+1] + 'e' + str(flat_num_array[i]).split('e')[1])
        except IndexError as error:
            try:
                out_array[index[0],index[1],index[2]] = float(str(flat_num_array[i])[0:n_mantis+1])
            except ValueError as error:
                out_array[index[0],index[1],index[2]] = flat_num_array[i]
    return out_array


def plot_lat_series(vars_list,lat_bnd,filename):
    plt.figure(1)
    for var in vars_list:
        plt.plot(lat[ll2ind(lat_bnd[0],lat):ll2ind(lat_bnd[1],lat)],\
            var[0,ll2ind(lat_bnd[0],lat):ll2ind(lat_bnd[1],lat),ll2ind(190,lon)])
    plt.savefig(os.path.join(image_directory,filename))
    plt.close()
    print('Plotted '+str(len(vars_list))+' variables to file: '+filename)

def scatter_plot(vars_list_x, vars_list_y, lon_bnd, filename, lat, lon, ylim=[-2,2], xlim=[-2,2]):
    plt.figure(1)
    for i in range(len(vars_list_x)):
        var_flat_x = vars_list_x[i][0,:,ll2ind(lon_bnd[0],lon):ll2ind(lon_bnd[1],lon)].flatten()
        var_flat_y = vars_list_y[i][0,:,ll2ind(lon_bnd[0],lon):ll2ind(lon_bnd[1],lon)].flatten()
        plt.scatter(var_flat_x, var_flat_y)
    plt.ylim(ylim)
    plt.xlim(xlim)
    plt.savefig(os.path.join(image_directory,filename))
    plt.close()




#######################################################################################################################
#
#                       Main Computation Functions: build NN func (NN_BUILD), calc viscosity from drifter(VISC_CALC),
#                               train NN on data(NN_TRAIN), calc eckman(ECK_CALC), calc semi-geos(SG_CALC)
#######################################################################################################################

def build_geos_NN():#NN_BUILD
    sla_c=Input(shape=(1,),name='sla_c')
    sla_N=Input(shape=(1,),name='sla_N')
    sla_S=Input(shape=(1,),name='sla_S')
    sla_E=Input(shape=(1,),name='sla_E')
    sla_W=Input(shape=(1,),name='sla_W')
    t_x=Input(shape=(1,),name='t_x')
    t_y=Input(shape=(1,),name='t_y')
    lat=Input(shape=(1,),name='lat')
    lon=Input(shape=(1,),name='lon')
    x=concatenate([sla_c,sla_N,sla_S,sla_E,sla_W,t_x,t_y,lat,lon])
    x=Dense(100, activation='relu')(x)
    x=Dense(300, activation='relu')(x)
    x=Dense(100, activation='relu')(x)

    output_uv = Dense(units=2, activation='linear', name='output_uv')(x)
    model = Model(inputs=[sla_c,sla_N,sla_S,sla_E,sla_W,t_x,t_y,lat,lon],outputs=output_uv)
    opt=keras.optimizers.Adam(lr=0.00001)

    model.compile(loss='mean_absolute_error', optimizer=opt, metrics=['mean_absolute_error'])
    return model



def drifter_viscos_calc(drifter_file, sg_file, eckman_file):#VISC_CALC
    ds_drift = nc.Dataset(drift_file, mode='r')
    ve = ds_drift['ve'][:,:]
    vn = ds_drift['vn'][:,:]
    time = ds_drift['time'][:]
    drift_lat = ds_drift['latitude'][:,:]
    drift_lon = ds_drift['longitude'][:,:]

    ds_sg = nc.Dataset(sg_file, mode='r+')
    ugsg = ds_sg['ugsg'][:,:,:]
    vgsg = ds_sg['vgsg'][:,:,:]
    ugos = ds_sg['ugos'][:,:,:]
    vgos = ds_sg['vgos'][:,:,:]
    time_sg = ds_sg['time'][:]
    lat = ds_sg['latitude'][:]
    lon = ds_sg['longitude'][:]


    ds_eck = nc.Dataset(eckman_file, mode='r+')
    tau_x = ds_eck['metss'][:,:,:]
    tau_y = ds_eck['mntss'][:,:,:]

    g = 9.80665
    a = 6.371e6
    beta = 2.3e-11
    omega = 7.2921e-5
    rho = 1025
    dtr = np.pi/180
    f = 2*omega*np.sin(dtr*lat)
    f = np.tile(f[None,:,None], (1,1,len(lon)))
    Lon, Lat = np.meshgrid(lon, lat)
    dx = a*np.cos(dtr*Lat)*dtr*np.gradient(Lon, axis=1)
    dy = a*dtr*np.gradient(Lat, axis=0)

    dx = dx[None,:]
    dy = dy[None,:]



    lon_5x5 = np.arange(lon[0],lon[-1],5)
    lat_5x5 = np.arange(lat[0],lat[-1],5)
    
    Lon_5x5, Lat_5x5 = np.meshgrid(lon_5x5,lat_5x5)

    ve_gridded = np.zeros((np.shape(ve)[0],np.shape(lat_5x5)[0],np.shape(lon_5x5)[0]))
    vn_gridded = np.zeros((np.shape(ve)[0],np.shape(lat_5x5)[0],np.shape(lon_5x5)[0]))

    for i in range(np.shape(ve)[0]):
        drift_lon_i = drift_lon[i,:]
        drift_lat_i = drift_lat[i,:]
        ve_i = ve[i,:]
        vn_i = vn[i,:]
        bool_mask_ve = (np.array(abs(drift_lon_i)<360) & np.array(abs(drift_lat_i)<90)) & np.array(abs(ve_i)<1e3)
        bool_mask_vn = (np.array(abs(drift_lon_i)<360) & np.array(abs(drift_lat_i)<90)) & np.array(abs(vn_i)<1e3)
        print(np.shape(drift_lon_i[bool_mask_ve]))
        print(np.shape(drift_lat_i[bool_mask_ve]))
        print(np.shape(ve_i[bool_mask_ve]))
        print(np.shape(ve_gridded))
        print(np.shape(Lon_5x5))
        ve_gridded[i,:,:] = interpolate.griddata((drift_lon_i[bool_mask_ve],drift_lat_i[bool_mask_ve]),\
                ve_i[bool_mask_ve], (Lon_5x5,Lat_5x5))
        vn_gridded[i,:,:] = interpolate.griddata((drift_lon_i[bool_mask_vn],drift_lat_i[bool_mask_vn]),\
                vn_i[bool_mask_vn], (Lon_5x5,Lat_5x5))
        print(i)
    print(np.shape(ve_gridded))
    
    time = time/24
    time_sg = np.arange(np.ceil(time[0]),np.floor(time[-1]),1)
    
    print(np.ceil(time[0]))
    print(np.floor(time[-1]))

    Time,Lat,Lon = np.meshgrid(time_sg,lat,lon)
    
    ve_grid_full = interpolate.interpn((time,lat_5x5,lon_5x5), ve_gridded, (Time,Lat,Lon))
    vn_grid_full = interpolate.interpn((time,lat_5x5,lon_5x5), vn_gridded, (Time,Lat,Lon))

    print(np.shape(ve_grid_full))


    
def test_geos_NN(sla_file,eckman_file):#NN_TRAIN


############################################################
#       test function for training basic NN in build_model #
#       input: semi_geos file                              #
#       output: trained model, saved weights               #
############################################################


#       Fixes:
#           - need to fix coastline by mirroring points
#           - work on imrpoving equator by shifting discontinuity to south africa




    ds=nc.Dataset(sla_file,mode='r')
    lat = ds['latitude'][:]
    lon = ds['longitude'][:]
    sla = ds['sla'][0:10,:,:]
    ugos = ds['ugos'][0:10,:,:]
    vgos = ds['vgos'][0:10,:,:]
    ugsg = ds['ugsg'][0:10,:,:]
    vgsg = ds['vgsg'][0:10,:,:]
    time_sg = ds['time'][0:10] 
    ds.close()


    ds_eckman = nc.Dataset(eckman_file, mode='r')
    t_x = ds_eckman['metss'][:,:,:]
    t_y = ds_eckman['mntss'][:,:,:]
    time_eckman = ds_eckman['time'][:]

    t_x = t_x[[int(i) for i in time_sg],:,:]
    t_y = t_y[[int(i) for i in time_sg],:,:]


    ds_eckman.close()

    sla_N = np.roll(sla, -1, axis=1)
    sla_S = np.roll(sla, 1, axis=1)
    sla_E = np.roll(sla, -1, axis=2)
    sla_W = np.roll(sla, 1, axis=2)

    lon[lon<180] = lon + 360

    Lon, Lat = np.meshgrid(lon,lat)
    
    Lon = np.tile(Lon[None,:,:], (np.shape(sla)[0],1,1))
    Lat = np.tile(Lat[None,:,:], (np.shape(sla)[0],1,1))
    
   
    a = 6.371e6
    dtr = np.pi/180

    dx = a*np.cos(dtr*Lat)*dtr*np.gradient(Lon, axis=2)
    dy = a*dtr*np.gradient(Lat, axis=1)

    print(np.shape(sla))
    print(np.shape(Lon))
    
    ind = ~(sla.mask | ugos.mask | vgos.mask | sla_N.mask | sla_S.mask | sla_E.mask | sla_W.mask)

    sla_f = sla[ind]
    sla_f = sla_f.data
    sla_f_N = sla_N[ind]
    sla_f_N = sla_f_N.data
    sla_f_S = sla_S[ind]
    sla_f_S = sla_f_S.data
    sla_f_E = sla_E[ind]
    sla_f_E = sla_f_E.data
    sla_f_W = sla_W[ind]
    sla_f_W = sla_f_W.data
    ugos_f = ugos[ind]
    ugos_f = ugos_f.data
    vgos_f = vgos[ind]
    vgos_f = vgos_f.data
    t_x_f = t_x[ind]
    t_x_f = t_x_f.data
    t_y_f = t_y[ind]
    t_y_f = t_y_f.data
    Lon_f = Lon[ind]
    Lon_f = Lon_f.data
    Lat_f = Lat[ind]
    Lat_f = Lat_f.data
    dx_f = dx[ind]
    dx_f = dx_f.data
    dy_f = dy[ind]
    dy_f = dy_f.data
    
    X_f = np.sin(dtr*Lat_f)
    Y_f = np.sin(dtr*Lon_f)*np.cos(dtr*Lat_f)

    print(np.shape(sla_f))
    print(np.shape(ugos_f))

    uv_target_f = np.stack((ugos_f,vgos_f),axis=-1)
    print(np.shape(uv_target_f))

    num_epochs = 20

    batch_size = 1000

    Model = build_geos_NN()

    history=Model.fit([sla_f,sla_f_N,sla_f_S,sla_f_E,sla_f_W,t_x_f,t_y_f,X_f,Y_f],[uv_target_f],batch_size=\
            batch_size,epochs=num_epochs,verbose=1, validation_split=0.1)
   
    Model.save_weights(os.path.join(model_weights,'geos_eck_model_weights.h5'))    
#    Model.load_weights(os.path.join(model_weights,'geos_eck_model_weights.h5'))


    ds=nc.Dataset(sla_file,mode='r')
    lat = ds['latitude'][:]
    lon = ds['longitude'][:]
    sla = ds['sla'][80:90,:,:]
    ugos = ds['ugos'][80:90,:,:]
    vgos = ds['vgos'][80:90,:,:]
    ugsg = ds['ugsg'][80:90,:,:]
    vgsg = ds['vgsg'][80:90,:,:]
    time_sg = ds['time'][80:90]

    ds_eckman = nc.Dataset(eckman_file, mode='r')
    t_x = ds_eckman['metss'][:,:,:]
    t_y = ds_eckman['mntss'][:,:,:]
    time_eckman = ds_eckman['time'][:]

    t_x = t_x[[int(i) for i in time_sg],:,:]
    t_y = t_y[[int(i) for i in time_sg],:,:]



    sla_N = np.roll(sla, -1, axis=1)
    sla_S = np.roll(sla, 1, axis=1)
    sla_E = np.roll(sla, -1, axis=2)
    sla_W = np.roll(sla, 1, axis=2)
    
    Lon, Lat = np.meshgrid(lon,lat)


    Lon = np.tile(Lon[None,:,:], (np.shape(sla)[0],1,1))
    Lat = np.tile(Lat[None,:,:], (np.shape(sla)[0],1,1))


    sla_test = sla[2,:,:].flatten()
    sla_N_test = sla_N[2,:,:].flatten()
    sla_S_test = sla_S[2,:,:].flatten()
    sla_E_test = sla_E[2,:,:].flatten()
    sla_W_test = sla_W[2,:,:].flatten()
    t_x_test = t_x[2,:,:].flatten()
    t_y_test = t_y[2,:,:].flatten()
    Lon_test = Lon[2,:,:].flatten()
    Lat_test = Lat[2,:,:].flatten()
    dx_test = dx[2,:,:].flatten()
    dy_test = dy[2,:,:].flatten()
    
    sla_test.filled(np.nan)
    sla_N_test.filled(np.nan)
    sla_S_test.filled(np.nan)
    sla_W_test.filled(np.nan)
    sla_E_test.filled(np.nan)
    t_x_test.filled(np.nan)
    t_y_test.filled(np.nan)
    Lon_test.filled(np.nan)
    Lat_test.filled(np.nan)


    X_test = np.sin(dtr*Lat_test)
    Y_test = np.sin(dtr*Lon_test)*np.cos(dtr*Lat_test)

    predicts_uv = Model.predict([sla_test,sla_N_test,sla_S_test,sla_E_test,sla_W_test,t_x_test,t_y_test,X_test,Y_test],\
            batch_size=batch_size,verbose=1)

    u_flat = predicts_uv[:,0]
    v_flat = predicts_uv[:,1]

    u = np.reshape(u_flat, (1,len(lat),len(lon)))
    v = np.reshape(v_flat, (1,len(lat),len(lon)))

    u = np.ma.masked_array(u, mask=ugos[2,:,:].mask)
    v = np.ma.masked_array(v, mask=vgos[2,:,:].mask)

    lon = ds['longitude'][:]

    Lon,Lat = np.meshgrid(lon,lat)

    print(np.shape(u))

    make_plot('u_geo_ek_NN_XY_360_dd82.png','u comp of total current (NN)', Lon, Lat, np.sqrt(u[:,:]**2 + v[:,:]**2), [0,0.5], cmocean.cm.ice)
    make_plot('u_NN_geo_ek_ugos_diff_XY360_dd82.png','u(NN) - ugos', Lon, Lat, abs(u[:,:] - ugos[2,:,:]), [0,0.5], cmocean.cm.ice)
    make_plot('ugos_dd82.png','ugos', Lon, Lat, np.sqrt(ugos[2,:,:]**2 + vgos[2,:,:]**2), [0,0.5], cmocean.cm.ice)
    scatter_plot([ugos[2:3,:,:]], [u], [45,120], 'u_NN_geoek_vs_ugos_XY360_dd82.png', lat, lon, ylim=[-1.5,1.5], xlim=[-1.5,1.5])
    scatter_plot([ugos[2:3,ll2ind(-90,lat):ll2ind(-10,lat),:]], [u[:,ll2ind(-90,lat):ll2ind(-10,lat),:]], [45,120],\
            'u_NN_geoek_vs_ugos_XY360_oe_dd82.png', lat, lon, ylim=[-1.5,1.5], xlim=[-1.5,1.5])
    quit()
    



#open and set up datasets
def compute_write_eckman(sg_file, eckman_file, stokes_file, write_file):#ECK_CALC

############################################################
#       eckman component calculation                       #
#       input: semi_geos file, wind stress (netcdf)        #
#       output: write eckman component to semi_geos file   #
############################################################


#       Fixes:
#           - Computes from K=8e-2



    ds_eckman = nc.Dataset(eckman_file, mode='r')
    t_x = ds_eckman['metss'][:,:,:]
    t_y = ds_eckman['mntss'][:,:,:]
    time_eckman = ds_eckman['time'][:]

    ds_stokes = nc.Dataset(stokes_file, mode='r')
    ust = ds_stokes['ust'][:,:,:]
    vst = ds_stokes['vst'][:,:,:]
    T_ww = ds_stokes['p1ww'][:,:,:]
    T_sw = ds_stokes['p1ps'][:,:,:]
    time_stokes = ds_stokes['time'][:]


    ds_sg = nc.Dataset(sg_file, mode='r')
    ugsg = ds_sg['ugsg'][:,:,:]
    vgsg = ds_sg['vgsg'][:,:,:]
    lat = ds_sg['latitude'][:]
    lon = ds_sg['longitude'][:]
    time_sg = ds_sg['time'][:]
    ugos = ds_sg['ugos'][:,:,:]
    vgos = ds_sg['vgos'][:,:,:]

    ds_w = nc.Dataset(write_file, mode='r+')

    
    g = 9.80665
    a = 6.371e6
    beta = 2.3e-11
    omega = 7.2921e-5
    rho = 1025
    dtr = np.pi/180
    f = 2*omega*np.sin(dtr*lat)
    f = np.tile(f[None,:,None], (len(time_sg),1,len(lon)))
    f = abs(f)
    K = 8e-2
    d = np.sqrt(2*K/f)
    Lon, Lat = np.meshgrid(lon, lat)
    dx = a*np.cos(dtr*Lat)*dtr*np.gradient(Lon, axis=1)
    dy = a*dtr*np.gradient(Lat, axis=0)

    dx = dx[None,:]
    dy = dy[None,:]


    ust = ust[[int(i) for i in time_sg],:,:]
    vst = vst[[int(i) for i in time_sg],:,:]
    T_ww = T_ww[[int(i) for i in time_sg],:,:]
    T_sw = T_sw[[int(i) for i in time_sg],:,:]

    t_x = t_x[[int(i) for i in time_sg],:,:]
    t_y = t_y[[int(i) for i in time_sg],:,:]
    print(d[30,ll2ind(-10,lat):ll2ind(10,lat),:])
    SS_bnd = [ll2ind(-90,lat),ll2ind(0,lat)]

    d_SS = d[:,SS_bnd[0]:SS_bnd[1],:]
    f_SS = f[:,SS_bnd[0]:SS_bnd[1],:]
    t_x_SS = t_x[:,SS_bnd[0]:SS_bnd[1],:]
    t_y_SS = t_y[:,SS_bnd[0]:SS_bnd[1],:]

    u_e = (t_x + t_y)/(rho*d*f)
    print(np.shape(u_e))
    u_e[:,SS_bnd[0]:SS_bnd[1],:] = (t_x_SS - t_y_SS)\
            /(rho*d_SS*f_SS)
    v_e = (t_y - t_x)/(rho*d*f)
    v_e[:,SS_bnd[0]:SS_bnd[1],:] = (t_x_SS + t_y_SS)\
            /(rho*d_SS*f_SS)



    u_e_20cm = np.sqrt(2)*np.exp(-0.2/d)*(t_x*np.cos((-0.2/d) - np.pi/4) - t_y*np.sin((-0.2/d) - np.pi/4))/(rho*d*f)
    u_e_20cm[:,SS_bnd[0]:SS_bnd[1],:] = np.sqrt(2)*np.exp(-0.2/d_SS)*(t_x_SS*\
            np.cos((-0.2/d_SS) - np.pi/4) + t_y_SS*np.sin((-0.2/d_SS) - np.pi/4))/\
            (rho*d_SS*f_SS)


    v_e_20cm = np.sqrt(2)*np.exp(-0.2/d)*(t_x*np.cos((-0.2/d) - np.pi/4) + t_y*np.sin((-0.2/d) - np.pi/4))/(rho*d*f)
    
    v_e_20cm[:,SS_bnd[0]:SS_bnd[1],:] = np.sqrt(2)*np.exp(-0.2/d_SS)*(t_x_SS*\
            np.cos((-0.2/d_SS) - np.pi/4) - t_y_SS*np.sin((-0.2/d_SS) - np.pi/4))/\
            (rho*d_SS*f_SS)


    u_e_15m = np.sqrt(2)*np.exp(-15/d)*(t_x*np.cos((-15/d) - np.pi/4) - t_y*np.sin((-15/d) - np.pi/4))/(rho*d*f)
    u_e_15m[:,SS_bnd[0]:SS_bnd[1],:] = np.sqrt(2)*np.exp(-15/d_SS)*(t_x_SS*\
            np.cos((-15/d_SS) - np.pi/4) + t_y_SS*np.sin((-15/d_SS) - np.pi/4))/\
            (rho*d_SS*f_SS)


    v_e_15m = np.sqrt(2)*np.exp(-15/d)*(t_x*np.cos((-15/d) - np.pi/4) + t_y*np.sin((-15/d) - np.pi/4))/(rho*d*f)
    
    v_e_15m[:,SS_bnd[0]:SS_bnd[1],:] = np.sqrt(2)*np.exp(-15/d_SS)*(t_x_SS*\
            np.cos((-15/d_SS) - np.pi/4) - t_y_SS*np.sin((-15/d_SS) - np.pi/4))/\
            (rho*d_SS*f_SS)

    

    ust_20cm = (1/2)*ust*(np.exp(8*(np.pi**2)*(-0.2)/(g*T_ww**2)) + np.exp(8*(np.pi**2)*(-0.2)/(g*T_sw**2)))
    print(ust[30,ll2ind(45,lat):ll2ind(60,lat),ll2ind(170,lon):ll2ind(190,lon)])
    print(T_ww[30,ll2ind(45,lat):ll2ind(60,lat),ll2ind(170,lon):ll2ind(190,lon)])
    print(ust_20cm[30,ll2ind(45,lat):ll2ind(60,lat),ll2ind(170,lon):ll2ind(190,lon)]/ust[30,ll2ind(45,lat):ll2ind(60,lat),ll2ind(170,lon):ll2ind(190,lon)])
    vst_20cm = (1/2)*vst*(np.exp(8*(np.pi**2)*(-0.2)/(g*T_ww**2)) + np.exp(8*(np.pi**2)*(-0.2)/(g*T_sw**2)))

    ust_15m = (1/2)*ust*(np.exp(8*(np.pi**2)*(-15)/(g*T_ww**2)) + np.exp(8*(np.pi**2)*(-15)/(g*T_sw**2)))
    vst_15m = (1/2)*vst*(np.exp(8*(np.pi**2)*(-15)/(g*T_ww**2)) + np.exp(8*(np.pi**2)*(-15)/(g*T_sw**2)))


    u_e_20cm = u_e_20cm - ust_20cm

    u_e_15m = u_e_15m - ust_15m




    Lon,Lat = np.meshgrid(lon,lat)

    make_plot('u_e.png','u_geos_eck - ugsg (m/s)',Lon, Lat, abs(u_e[30,:,:]),[0,0.5],cmocean.cm.ice)
    make_plot('abs_speed_st.png', 'u_f (m/s)', Lon, Lat, np.sqrt(ust[30,:,:]**2 + vst[30,:,:]**2),[0,0.5],\
            cmocean.cm.ice)
    make_plot('abs_speed_gsg_eck_st_20cm.png', 'ugsg (m/s)', Lon, Lat, np.sqrt((u_e_20cm[40,:,:] + ugsg[40,:,:] + \
            ust_20cm[40,:,:])**2 + (v_e_20cm[40,:,:] + vgsg[40,:,:] + vst_20cm[40,:,:])**2),[0,0.5],cmocean.cm.ice)

   
    #Create variables
    try:
        u_ek_20cm = ds_w.createVariable('u_20cm', np.float32, ('time', 'latitude', 'longitude'),fill_value=-32767)
        u_ek_20cm.units='m/s'
        u_ek_20cm.standard_name='surface_eastward_sea_water_velocity(m/s)'
        u_ek_20cm.missing_value = -32767
        u_ek_20cm.long_name = 'Absolute Geostrophic, Ekman, and Stokes Velocity at 20cm: Zonal Component'
        u_ek_20cm.grid_mapping = 'crs'
        u_ek_20cm.coordinates = 'longitude latitude'

        v_ek_20cm = ds_w.createVariable('v_20cm', np.float32, ('time', 'latitude', 'longitude'),fill_value=-32767)
        v_ek_20cm.units='m/s'
        v_ek_20cm.standard_name='surface_northward_sea_water_velocity(m/s)'
        v_ek_20cm.missing_value = -32767
        v_ek_20cm.long_name = 'Absolute Geostrophic, Ekman, and Stokes Velocity at 20cm: Meridional Component'
        v_ek_20cm.grid_mapping = 'crs'
        v_ek_20cm.coordinates = 'longitude latitude'

        u_ek_15m = ds_w.createVariable('u_15m', np.float32, ('time', 'latitude', 'longitude'),fill_value=-32767)
        u_ek_15m.units='m/s'
        u_ek_15m.standard_name='eastward_sea_water_velocity(m/s)'
        u_ek_15m.missing_value = -32767
        u_ek_15m.long_name = 'Absolute Geostrophic, Ekman, and Stokes Velocity at 15m: Zonal Component'
        u_ek_15m.grid_mapping = 'crs'
        u_ek_15m.coordinates = 'longitude latitude'

        v_ek_15m = ds_w.createVariable('v_15m', np.float32, ('time', 'latitude', 'longitude'),fill_value=-32767)
        v_ek_15m.units='m/s'
        v_ek_15m.standard_name='northward_sea_water_velocity(m/s)'
        v_ek_15m.missing_value = -32767
        v_ek_15m.long_name = 'Absolute Geostrophic, Ekman, and Stokes Velocity at 15m: Meridional Component'
        v_ek_15m.grid_mapping = 'crs'
        v_ek_15m.coordinates = 'longitude latitude'

        
        

        print('Successfully wrote 4 variables to '+write_file)
    
    except Exception as e:
        print(e)


    u_ek_20cm[:,:,:] = u_e_20cm + ugsg + ust_20cm
    v_ek_20cm[:,:,:] = v_e_20cm + vgsg + vst_20cm

    u_ek_15m[:,:,:] = u_e_15m + ugsg + ust_15m
    v_ek_15m[:,:,:] = u_e_15m + vgsg + vst_15m

    ds_eckman.close()
    ds_sg.close()
    ds_stokes.close()
    ds_w.close()


def compute_write_sg(filename):#SG_CALC


############################################################
#       semi_geos component calculation                    #
#       input: altimetry file (netcdf)                     #
#       output: write semi-geos to write out file          #
############################################################


    ds = nc.Dataset(filename, mode='r+')
    sla = ds['sla'][:,:,:]
    ugos = ds['ugos'][:,:,:]
    vgos = ds['vgos'][:,:,:]
    lat = ds['latitude'][:]
    lon = ds['longitude'][:]
    time = ds['time'][:]

    g = 9.80665
    a = 6.371e6
    omega = 7.2921e-5
    dtr = np.pi/180
    f = 2*omega*np.sin(dtr*lat)
    f = np.tile(f[None,:,None], (len(time),1,len(lon)))
    beta = 2*omega*np.cos(dtr*lat)
    beta = np.tile(beta[None,:,None], (len(time),1,len(lon)))
    Lon, Lat = np.meshgrid(lon, lat)
    dx = a*np.cos(dtr*Lat)*dtr*np.gradient(Lon, axis=1)
    dy = a*dtr*np.gradient(Lat, axis=0)

    dx = dx[None,:]
    dy = dy[None,:]
    
    print(np.shape(f))
    print(np.shape(beta))
    #Geostrophic off-equator
    dsladx = np.gradient(sla, axis=2)
    dslady = np.gradient(sla, axis=1)

    dsladx = dsladx/dx
    dslady = dslady/dy 
    
    #out of use code for adding offset to geostrophic

    #A = dslady[:,ll2ind(0,lat),:]
    #print(np.shape(A))
    #A[A*0 != 0] = 0
    #sigma = 1
    #A = np.tile(A[:,None,:],(1,len(lat),1))
    #print(np.shape(A))
    #offset_der = -A*np.exp(-Lat**2/sigma**2) + 2*A*(Lat**2)*np.exp((-Lat**2)/(sigma**2))/(sigma**2)

    dsla_corr_dy = dslady



    u_geo_oe = -g*dsla_corr_dy/f
    v_geo_oe = g*dsladx/f


    #semi-geostrophic on-equator

    ddsladx = np.gradient(dsladx, axis=2)
    ddslady = np.gradient(dslady, axis=1)
    ddsladxdy = np.gradient(dslady, axis=2)
    ddsladx = ddsladx/dx
    ddslady = ddslady/dy
    ddsladxdy = ddsladxdy/dx


    #apply smoothing to semi-geostrophic

    conv_kernel = np.array([0.08181154684764567,\
                            0.10170064040322004,\
                            0.11880379795740208,\
                            0.1304165914162576,\
                            0.13453484675094923,\
                            0.1304165914162576,\
                            0.11880379795740208,\
                            0.10170064040322004,\
                            0.08181154684764567])

    conv_kernel = conv_kernel[:,None]

    
    ddslady[ddslady*0 != 0 ] = 0
    ddsladxdy[ddsladxdy*0 != 0] = 0

    for j in range(len(time)):
        for i in range(15):
            ddslady[j,ll2ind(-5,lat):ll2ind(5,lat),:] =signal.convolve2d(ddslady[j,ll2ind(-5,lat):ll2ind(5,lat),:],\
                                                                                            conv_kernel,'same')
            ddsladxdy[j,ll2ind(-5,lat):ll2ind(5,lat),:] =signal.convolve2d(ddsladxdy[j,ll2ind(-5,lat):ll2ind(5,lat),:],\
                                                                                            conv_kernel, 'same')
    print('Progress: '+str(j)+'/'+str(len(time)))


    u_sg_e = -g*ddslady/beta
    v_sg_e = g*ddsladxdy/beta

    #Create variables
    try:
        u_geo_nc = ds.createVariable('ugsg', np.float32, ('time', 'latitude', 'longitude'), fill_value=-2147483648)
        u_geo_nc.units='m/s'
        u_geo_nc.standard_name='eastward_geostrophic_seawater_velocity(m/s)'
        u_geo_nc.missing_value = -2147483648
        u_geo_nc.scale_factor = 0.0001
        u_geo_nc.valid_min = -47959
        u_geo_nc.valid_max = 49770
        u_geo_nc.long_name = 'geostrophic and ageostrophic estimate to zonal surface velocity'
        u_geo_nc.grid_mapping = 'crs'
        u_geo_nc.coordinates = 'longitude latitude'

        v_geo_nc = ds.createVariable('vgsg', np.float32, ('time', 'latitude', 'longitude'), fill_value=-2147483648)
        v_geo_nc.units='m/s'
        v_geo_nc.standard_name='northward_geostrophic_seawater_velocity(m/s)'
        v_geo_nc.missing_value = -2147483648
        v_geo_nc.scale_factor = 0.0001
        v_geo_nc.valid_min = -49865
        v_geo_nc.valid_max = 49073
        v_geo_nc.long_name = 'geostrophic and ageostrophic estimate to meridional surface velocity'
        v_geo_nc.grid_mapping = 'crs'
        v_geo_nc.coordinates = 'longitude latitude'
    
    except:
        pass

    amp = 1.0
    waist = 2.2 
    gauss_kernel = np.exp(-Lat**2/(waist**2))
    comp_gauss_kernel = 1 - gauss_kernel
    gauss_kernel = amp*gauss_kernel

    u_geo_f = u_geo_oe*comp_gauss_kernel + u_sg_e*gauss_kernel
    v_geo_f = v_geo_oe*comp_gauss_kernel + v_sg_e*gauss_kernel


# Add Ageos/advective component

    du_geo_dx = np.gradient(u_geo_f, axis=2)
    du_geo_dx = du_geo_dx/dx

    dv_geo_dy = np.gradient(v_geo_f, axis=1)
    dv_geo_dy = dv_geo_dy/dy


    u_f = (-f*v_geo_f*dv_geo_dy + (u_geo_f*f**2))/(f**2 + du_geo_dx*dv_geo_dy)
    v_f = (-f*u_geo_f*du_geo_dx + (v_geo_f*f**2))/(f**2 + du_geo_dx*dv_geo_dy)


#    u_geo_nc[:,:,:] = u_f
#    v_geo_nc[:,:,:] = v_f

    print(np.amax(u_f))

    make_plot('abs_ageos_geos_speed.png', 'sqrt(u_f^2 + v_f^2)', Lon, Lat, np.sqrt(u_geo_f[40,:,:]**2 + v_geo_f[40,:,:]**2),\
            [0,0.6],cmocean.cm.ice)

    ds.close()





#####################################---------Main File-------########################################################





file_eckman = os.path.join(data_directory,'era5-stress-dy-2019_remap.nc')
sg_file = os.path.join(data_directory, 'rads_global_nrt_sla_2019.nc')
drift_file = os.path.join(data_directory,'mon_avg_test.nc')
write_file = os.path.join(data_directory,'global_BOSC_SGE_2Lev_2019.nc')
file_oscar = 'world_oscar_vel_5d2019.nc'
stokes_file = os.path.join(data_directory,'era5-stokes-dy-2019_regrid.nc')
glob_file = os.path.join(data_directory, 'dataset-uv-rep-daily_2019_merge_remap.nc')

try:
    file_eckman = os.path.join(data_directory,e_file)
    sg_file = os.path.join(data_directory, g_file)
    write_file = os.path.join(data_directory, w_file)
    stokes_file = os.path.join(data_directory, s_file)

except NameError as e:
    print(e)

print(write_file)
print(sg_file)
print(file_eckman)
print(stokes_file)


#perform calculations
print(tf.config.list_physical_devices('CPU'))
compute_write_sg(sg_file)
#test_geos_NN(sg_file,file_eckman)
#quit()
compute_write_eckman(sg_file,file_eckman,stokes_file, write_file)

#plot total and geo component of surface current estimates

ds = nc.Dataset(write_file, mode='r')
ugeoek = ds['u_15m'][:,:,:]
vgeoek = ds['v_15m'][:,:,:]
lat = ds['latitude'][:]
lon = ds['longitude'][:]
Lon,Lat = np.meshgrid(lon,lat)

ds_glob = nc.Dataset(glob_file, mode='r')
u_glob = ds_glob['uo'][:,:,:,:]
v_glob = ds_glob['vo'][:,:,:,:]


make_plot('glob_curr_total_speed.png','absolute speed (GlobCurr) (m/s)', Lon, Lat, np.sqrt(u_glob[130,0,:,:]**2 + \
        v_glob[130,0,:,:]**2), [0,0.5], cmocean.cm.ice)

make_plot('speed_total_est_2019_DD40.png','absolute speed (G + E + S) (m/s)', Lon, Lat, np.sqrt(ugeoek[40,:,:]**2 + \
        vgeoek[40,:,:]**2), [0,0.5], cmocean.cm.ice)

make_plot('glob_BOSC_diff.png','globcurrent - BOSC (m/s)', Lon, Lat, np.sqrt((ugeoek[40,:,:] - \
        u_glob[130,1,:,:])**2 + (vgeoek[40,:,:] - v_glob[130,1,:,:])**2), [0,0.5], cmocean.cm.ice)

ds.close()

quit()
