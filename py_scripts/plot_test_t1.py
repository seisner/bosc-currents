import numpy as np
import netCDF4 as nc
from mpl_toolkits.basemap import Basemap,shiftgrid
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from skimage.transform import resize
import datetime

import pickle as cPickle
import os.path




def read_nc_diag(filename):

 diag_full=nc.Dataset(filename,'r')
 obs=diag_full.variables['Observation'][:]
 lat=diag_full.variables['Latitude'][:]
 lon=diag_full.variables['Longitude'][:]
 omb=diag_full.variables['Obs_Minus_Forecast_unadjusted'][:]
 ichannel=diag_full.variables['Channel_Index'][:]

 mxch=int(np.max(ichannel))
 idx=(ichannel == 1)
 nobs=idx.sum()

 ichan=np.arange(1,mxch+1)  # all channles
 nchan=ichan.shape

 tb=np.zeros((nchan[0],nobs))
 omboma=np.zeros((nchan[0],nobs))
 qc=np.zeros((nchan[0],nobs))

 t=0
 for ich in ichan:

  idx=((ichannel == ich))
  if (idx.sum() > 0):
   tb[t,:]=obs[idx]
   omboma[t,:] = omb[idx]
   dlat=lat[idx]
   dlon=lon[idx]
   t=t+1
 return tb,omboma,dlat,dlon
 
def read_nc_nemiso(filename,varname,lev):
  ncf = nc.Dataset(filename)
  var = ncf.variables[varname][:]
  var=var[0,:,:,:]
  varlev = var[lev,...]
  pfull = ncf.variables['pfull'][:]
  lat = ncf.variables['lat'][:]
  lon = ncf.variables['lon'][:]
  lon[lon > 180] = lon[lon > 180] - 360.
#  lons, lats = np.meshgrid(lon,lat)
  return lon,lat,varlev,pfull

def read_nc_spread(filename,varname,lev):
  ncf = nc.Dataset(filename)
  var = ncf.variables[varname][:]
  var=var[0,:,:,:]
  varlev = var[lev,...]
  lat = ncf.variables['lat'][:]
  lon = ncf.variables['lon'][:]
  lon[lon > 180] = lon[lon > 180] - 360.
#  lons, lats = np.meshgrid(lon,lat)
  return lon,lat,varlev

def make_plot(imgname,title,llon,llat,dvar,clim,cmap,tickformat):

 m = Basemap(llcrnrlon=-180, llcrnrlat=-90, urcrnrlon=180, urcrnrlat=90, projection='cyl')
 fig = plt.figure()
 m.drawcoastlines()
 m.drawcountries()
 m.drawmeridians(np.arange(-180.,180.,60.),labels=[True,False,False,False])
 m.drawparallels(np.arange(-90.,91.,30.),labels=[False,False,True,False])


 minv  = clim[0]
 maxv  = clim[1]
 ticks = np.linspace(minv,maxv,10)

 llon[llon > 180] = llon[llon > 180] - 360.

 cs = m.scatter(llon,llat,2,dvar,latlon=False,vmin=minv,vmax=maxv,cmap=cmap)
 cbar = m.colorbar(cs, location='bottom', pad="10%",extend='both',ticks=ticks,format=tickformat)
# cbar.formatter.set_powerlimits((0, 0))

 plt.title(title)
 plt.savefig(imgname,bbox='tight')
 plt.close('all')

def plot_2dhist(x, y, title,imgname):

 mxx = x.max()
 mxy = y.max()
 nxx = x.min()
 nxy = y.min()
 mxx = max([mxx,mxy])
 nxx = min([nxx,nxy])
 rangexy = [[nxx,mxx],[nxx,mxx]]
 nbins=100
 H, xedges, yedges = np.histogram2d(x,y, bins=nbins, range=rangexy, normed=False)
 H = np.rot90(H)
 H = np.flipud(H)
 Hn = H/np.sum(H)
# print np.min(Hn), np.max(Hn)
# print np.min(H), np.max(H), np.sum(H), len(x)
 Hmasked = np.ma.masked_where(H==0,H) # Mask pixels with a value of zero
 cmap=plt.cm.jet
 fig = plt.figure()
 ax = fig.add_axes([0.1, 0.1, 0.8, 0.8])
 plt.pcolormesh(xedges,yedges,Hmasked,cmap=cmap,alpha=0.99)
 plt.xlabel('Truth')
 plt.ylabel('NN')
 plt.title(title)
 cbar = plt.colorbar()
 cbar.ax.set_ylabel('Density of Points')

 plt.rcParams.update({'figure.autolayout': True})

 dy = 0.04 ; y0 = 0.9
 x0 = 0.185


 dxx = x - y
 x1=np.round(x*100,2)
 y1=np.round(y*100,2)
 rcoeff = np.corrcoef(x1,y1*100)[0][1]
 tcolor='black' #'dimgrey'
 tweight='' #'bold'
 str1 = 'Npts :%9d' % len(x)
 plt.text(0.31, y0,str1, ha='right',va='center', transform=ax.transAxes,color=tcolor)#,weight=tweight)
 str2 = 'Corr : %9.3f' % rcoeff
 plt.text(0.31, y0-dy,str2, ha='right',va='center', transform=ax.transAxes,color=tcolor)#,weight=tweight)
 str3 = 'bias : %9.3f' % np.mean(dxx)
 plt.text(0.31, y0-2*dy,str3, ha='right',va='center', transform=ax.transAxes,color=tcolor)#,weight=tweight)
 str4 = 'Sdv : %9.3f' % np.std(dxx)
 plt.text(0.31, y0-3*dy,str4, ha='right', va='center', transform=ax.transAxes,color=tcolor)#,weight=tweight)

 plt.savefig(imgname,bbox='tight')
 fig.clf()
 plt.close('all')

def plot_stats(x,y,pfull,title,imgname):

 bt=np.zeros(pfull.shape[0])
 bs=np.zeros(pfull.shape[0]) 
 for i in range(0,pfull.shape[0]):
  bt[i]=np.mean(x[:,i]-y[:,i])
  bs[i]=np.std(x[:,i]-y[:,i])

 plev=np.round(pfull,0)
 print(plev)
 fig, ax = plt.subplots()
 ax.plot(bt,plev,label='bias',color='blue')
 ax.plot(bs,plev,label='std',color='blue',linestyle='--')
# ax.set_yscale('log')
 ax.set_xlim([-1,1])
 ax.set_ylim([1000.,100.])
 ax.set_xlabel('T (K)')
 ax.set_ylabel('Pressure, [hPa]')
 ax.set_title(title)
 ax.grid(True,which="both")
 ax.legend()
 plt.savefig(imgname)
 plt.close() 


def coloc_inc(llon,llat,data):

 llon[llon < 0] = llon[llon < 0] + 360.

 [nlat,nlon]=np.shape(data)
 
 minLatNWP=90
 maxLatNWP=-90
 minLonNWP=0
 maxLonNWP=359

 factlat=-180./nlat
 factlon=360./nlon

 NWPlat=np.zeros(nlat)
 NWPlon=np.zeros(nlon)

 for ix in range(0,nlat-1):
  NWPlat[ix]=90. + factlat*(ix-1)

 for iy in range(0,nlon-1):
  NWPlon[iy]=0. + factlon*(iy-1)

 ilat=(llat-minLatNWP)/factlat
 ilon=(llon-minLonNWP)/factlon

 ilat = ilat.astype(int)
 ilon = ilon.astype(int)


 ilat[ilat > nlat] = nlat-1
 ilat[ilat < 1]  = 1

 ilon[ilon > nlon] = nlon-1
 ilon[ilon < 1] = 1


 ilat0  = ilat
 ilat1  = ilat +1
 ilon0  = ilon
 ilon1  = ilon +1


 ilat1[ilat1 > nlat-1] = nlat-1               #ilat1[ilat1 > nlat] = nlat-1
 ilat1[ilat1 < 1]  = 1

 ilon1[ilon1 > nlon-1] = 1
 ilon1[ilon1 < 1] = 1


 fraclat = (llat-NWPlat[ilat0])/(NWPlat[ilat1]-NWPlat[ilat0])
 fraclon = (llon-NWPlon[ilon0])/(NWPlon[ilon1]-NWPlon[ilon0])


 TisInterInlat1 = data[ilat0,ilon0]+fraclat*(data[ilat0,ilon1]-data[ilat0,ilon0])
 TisInterInlat2 = data[ilat1,ilon0]+fraclat*(data[ilat1,ilon1]-data[ilat1,ilon0])
 vcol = TisInterInlat1+fraclon*(TisInterInlat2-TisInterInlat1)
 return vcol


def prep_data(data_path,yyyy,mm,sday,eday,shour,ehour):


 for day in range(sday,eday):
  for hour in range(shour,ehour):
 
    dd=day
    hh=hours[hour]

    anl_time = datetime.datetime(yyyy, mm, dd, hh, 0, 0)

    adate=str(anl_time.year)+str(anl_time.strftime("%m"))+str(anl_time.strftime("%d"))
    ahour=str(anl_time.strftime("%H"))
    print(adate,ahour,flush=True)

    h6=datetime.timedelta(hours=6)
    ges_time = anl_time - h6

    gdate=str(ges_time.year)+str(ges_time.strftime("%m"))+str(ges_time.strftime("%d"))
    ghour=str(ges_time.strftime("%H"))

    file_diag_ges=data_path+'/diag_atms_n20_ges.'+adate+ahour+'.nc4'
    file_nemsio_anl=data_path+'/atmanl.gdas.'+adate+ahour+'.nc'
    file_nemsio_ges=data_path+'/atmf006.gdas.'+gdate+ghour+'.nc'

    print(file_diag_ges,file_nemsio_anl,file_nemsio_ges,flush=True)
    [TB,omb,dlat,dlon]=read_nc_diag(file_diag_ges)
    TB[TB>1000]=231

    nobs=dlat.shape
    Ts_anl_all=np.zeros((nobs[0],64))
    Ts_ges_all=np.zeros((nobs[0],64))

    for lay in range(0,64):
      [lon,lat,Ts_anl,pfull]=read_nc_nemiso(file_nemsio_anl,'tmp',lay)
      [lon,lat,Ts_ges,pfull]=read_nc_nemiso(file_nemsio_ges,'tmp',lay)
      Ts_anl_coloc=coloc_inc(dlon,dlat,Ts_anl)
      Ts_ges_coloc=coloc_inc(dlon,dlat,Ts_ges)
      Ts_anl_all[:,lay]=Ts_anl_coloc
      Ts_ges_all[:,lay]=Ts_ges_coloc


    print(TB.shape,Ts_ges_all.shape,Ts_anl_all.shape,flush=True)
    print(np.min(Ts_ges_all),np.max(Ts_ges_all),flush=True)
    print(np.min(Ts_anl_all),np.max(Ts_anl_all),flush=True)

 return Ts_anl_all,Ts_ges_all,dlat,dlon,pfull

model='hype_tune_test_2_val_9_train5'
var='T_'

img_path='/work/noaa/dras-aida/seisner/test-outputs/images/'
data_path='/work/noaa/dras-aida/nshahrou/ROTDIRS/noscrub/archive/TEST_atms/'
aifile_path='/work/noaa/dras-aida/seisner/test-outputs/'+model

yyyy=2019
mm=8
hours=[0,6,12,18]
sday=9
eday=10
shour=0
ehour=1

anl_time = datetime.datetime(yyyy, mm, sday, shour, 0, 0)
date1=str(anl_time.year)+str(anl_time.strftime("%m"))+str(anl_time.strftime("%d"))+str(anl_time.strftime("%H"))

Ts_anl_all1,Ts_ges_all1,dlat1,dlon1,pfull=prep_data(data_path,yyyy,mm,sday,eday,shour,ehour)


sday=2
eday=3
shour=0
ehour=1

anl_time = datetime.datetime(yyyy, mm, sday, shour, 0, 0)
date2=str(anl_time.year)+str(anl_time.strftime("%m"))+str(anl_time.strftime("%d"))+str(anl_time.strftime("%H"))

Ts_anl_all2,Ts_ges_all2,dlat2,dlon2,pfull=prep_data(data_path,yyyy,mm,sday,eday,shour,ehour)


filename=aifile_path+'/dnn_test.nn.pkl'
with open(filename,'rb') as fp:
  Tnn1 = cPickle.load(fp)


filename=aifile_path+'/dnn_train.nn.pkl'
with open(filename,'rb') as fp:
  Tnn2 = cPickle.load(fp)

print(Tnn1.shape)
print(np.min(Ts_anl_all1),np.max(Ts_anl_all1))
print(np.min(Tnn1),np.max(Tnn1))

print(np.min(Ts_anl_all2),np.max(Ts_anl_all2))
print(np.min(Tnn2),np.max(Tnn2))

print(pfull)


layers=[250,500,850]
lays=[31,39,51]

for l in range(0,3):
 lay=lays[l] 
 layer=layers[l] 

# title=var+str(layer)+'hPa_anl_NN_'+date1+'_'+model+'_indep'
# imgname=img_path+title
# cmap  = plt.cm.jet
# clim=[np.min(Tnn1[:,lay]),np.max(Tnn1[:,lay])]
# tickformat="%5.0f"
# make_plot(imgname,title,dlon1,dlat1,Tnn1[:,lay],clim,cmap,tickformat)


# title=var+str(layer)+'hPa_anl_gdas_'+date1+'_'+model+'_indep'
# imgname=img_path+title
# cmap  = plt.cm.jet
# clim=[np.min(Tnn1[:,lay]),np.max(Tnn1[:,lay])]
# tickformat="%5.0f"
# make_plot(imgname,title,dlon1,dlat1,Ts_anl_all1[:,lay],clim,cmap,tickformat)

# Tinc1=(Tnn1[:,lay]-Ts_ges_all1[:,lay])
 Tinc1=Tnn1[:,lay]
 Ts_inc_all1=Ts_anl_all1[:,lay]-Ts_ges_all1[:,lay]

 print(np.min(Tinc1),np.max(Tinc1))
 print(np.min(Ts_inc_all1),np.max(Ts_inc_all1))

 title=var+str(layer)+'hPa_inc_NN_'+date1+'_'+model+'_indep'
 imgname=img_path+title
 cmap  = plt.cm.bwr
 clim=[-1,1]
 tickformat="%5.1f"
 make_plot(imgname,title,dlon1,dlat1,Tinc1,clim,cmap,tickformat)

 title=var+str(layer)+'hPa_inc_gdas_'+date1+'_'+model+'_indep'
 imgname=img_path+title
 cmap  = plt.cm.bwr
 clim=[-1,1]
 tickformat="%5.1f"
 make_plot(imgname,title,dlon1,dlat1,Ts_inc_all1,clim,cmap,tickformat)

 T_diff1=Tinc1-Ts_inc_all1
 title=var+str(layer)+'hPa_diff_'+date1+'_'+model+'_indep'
 imgname=img_path+title
 cmap  = plt.cm.bwr
 clim=[np.min(T_diff1),np.max(T_diff1)]
 clim=[-1,1]
 tickformat="%5.1f"
 make_plot(imgname,title,dlon1,dlat1,T_diff1,clim,cmap,tickformat)

 title=var+str(layer)+'hPa_inc_scatter_'+date1+'_'+model+'_indep'
 imgname=img_path+title
 plot_2dhist(Ts_inc_all1, Tinc1, title,imgname)
 
##Train

# title=var+str(layer)+'hPa_anl_NN_'+date2+'_'+model+'_train'
# imgname=img_path+title
# cmap  = plt.cm.jet
# clim=[np.min(Tnn2[:,lay]),np.max(Tnn2[:,lay])]
# tickformat="%5.0f"
# make_plot(imgname,title,dlon2,dlat2,Tnn2[:,lay],clim,cmap,tickformat)

# title=var+str(layer)+'hPa_anl_gdas_'+date2+'_'+model+'_train'
# imgname=img_path+title
# cmap  = plt.cm.jet
# clim=[np.min(Tnn2[:,lay]),np.max(Tnn2[:,lay])]
# tickformat="%5.0f"
# make_plot(imgname,title,dlon2,dlat2,Ts_anl_all2[:,lay],clim,cmap,tickformat)

# Tinc2=(Tnn2[:,lay]-Ts_ges_all2[:,lay])# /10
 Tinc2=Tnn2[:,lay]
 Ts_inc_all2=Ts_anl_all2[:,lay]-Ts_ges_all2[:,lay]

 print(np.min(Tinc2),np.max(Tinc2))
 print(np.min(Ts_inc_all2),np.max(Ts_inc_all2))

 title=var+str(layer)+'hPa_inc_NN_'+date2+'_'+model+'_train'
 imgname=img_path+title
 cmap  = plt.cm.bwr
 clim=[-1,1]
 tickformat="%5.1f"
 make_plot(imgname,title,dlon2,dlat2,Tinc2,clim,cmap,tickformat)

 title=var+str(layer)+'hPa_inc_gdas_'+date2+'_'+model+'_train'
 imgname=img_path+title
 cmap  = plt.cm.bwr
 #clim=[-2,2]
 tickformat="%5.1f"
 make_plot(imgname,title,dlon2,dlat2,Ts_inc_all2,clim,cmap,tickformat)

 T_diff2=Tinc2-Ts_inc_all2
 title=var+str(layer)+'hPa_diff_'+date2+'_'+model+'_train'
 imgname=img_path+title
 cmap  = plt.cm.bwr
 #clim=[np.min(T_diff2),np.max(T_diff2)]
 #clim=[-2,2]
 tickformat="%5.1f"
 make_plot(imgname,title,dlon2,dlat2,T_diff2,clim,cmap,tickformat)
 title=var+str(layer)+'hPa_inc_scatter_'+date2+'_'+model+'_train'
 imgname=img_path+title
 plot_2dhist(Ts_inc_all2, Tinc2, title,imgname)


x1=Ts_anl_all1-Ts_ges_all1
y1=Tnn1
#-Ts_ges_all1
#title=var+'_stats_'+date1+'_'+model+'_indep_inc'
title=var+'stats_AI_inc_vs_GDAS_inc_'+model+'_indep'
imgname=img_path+title
plot_stats(x1,y1,pfull,title,imgname)


x2=Ts_anl_all2-Ts_ges_all2
y2=Tnn2
#-Ts_ges_all2
#title=var+'_stats_'+date2+'_'+model+'_train_inc'
title=var+'stats_AI_inc_vs_GDAS_inc_'+model+'_train'
imgname=img_path+title
plot_stats(x2,y2,pfull,title,imgname)
quit()


#x1=Ts_anl_all1
#y1=Tnn1
#title=var+'stats_'+date1+'_'+model+'_indep_inc'
#title=var+'stats_AI_anal_vs_GDAS_anal_indep'
#imgname=img_path+title
#plot_stats(x1,y1,pfull,title,imgname)


#x2=Ts_anl_all2
#y2=Tnn2

#title=var+'stats_'+date2+'_'+model+'_train_inc'
#title=var+'stats_AI_anal_vs_GDAS_anal_train'
#imgname=img_path+title
#plot_stats(x2,y2,pfull,title,imgname)
